/**
 * Created by timur on 21.02.17.
 */
import Items from '../containers/Items';
import React, {Component} from 'react';
import ShoppingCart from '../containers/ShoppingCart';
import './App.css';


export default class  App extends Component {

    render() {
        return (
            <div className="App-mainbody">
                    <Items />
                <ShoppingCart />
            </div>
        );
    }
}

